#!/bin/sh
set -e

VERSION=0.1
NAME=felix-antora-doc
mkdir -p build/site/${NAME}/${VERSION}/
asciidoctor-pdf modules/ROOT/pages/_${NAME}.adoc -o build/site/${NAME}/${VERSION}/${NAME}.pdf
