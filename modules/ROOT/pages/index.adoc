= FELIX Documentation using Antora

This document describes how FELIX uses the Antora system to
assembles its documentation.

ifndef::backend-pdf[]
A link:felix-antora-doc.pdf[PDF] version is available.
endif::[]

FIXME More to come... 

Documentation is writen in https://asciidoctor.org/docs/what-is-asciidoc/[AsciiDoc] format in one or more files, making up a book, or set of related documents. These documents are converted into html or pdf using https://asciidoctor.org[Asciidoctor]. One of more books (or set of files) or one or more versions of these books can be combined into a website with menus, navigation and indices using https://antora.org[Antora].

== Directory structure

Move your source adoc files (index.adoc, ...) into directory

****
modules/ROOT/pages/
****

move your image files (8.svg, *.png, ...) into directory

****
modules/ROOT/images/
****

to build the navigation on the left side of the page craete a file

****
modules/ROOT/nav.adoc
****

with xrefs like these

....
 * xref:index.adoc[Introduction]
....

The navigation panel on the right side is constructed from the headings in your adoc files.

Create an antora.yml at the root dir of your doc project with the following content to declare your documentation module.

....
name: <project-name>
title: <title of your documemtation>
version: '<version>'
start_page: ROOT:index.adoc
nav:
- modules/ROOT/nav.adoc
....
where you replace <project-name>, <title of your documemtation> and <version>.
Version has to be quoted.

Now your documentation module can be included into other docs. To be able to convert it also stand-alone you need to create an antora-playbook.yml file at the root dir of your doc project with the content.

....
site:
  title: <title of your documemtation>
  start_page: <project name>::index.adoc
content:
  sources:
  - url: ./
    branches: HEAD
asciidoc:
  attributes:
    experimental: ''
    idprefix: ''
    idseparator: '-'
    page-pagination: ''
    source-language: asciidoc@
ui:
  bundle:
    url: https://gitlab.cern.ch/atlas-tdaq-felix/felix-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable
    snapshot: true
....

where you replace <title of your documemtation> and <project name>.
