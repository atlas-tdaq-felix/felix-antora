#!/bin/sh
set -e

export DOCSEARCH_ENABLED=true
export DOCSEARCH_ENGINE=lunr
export NODE_PATH="$(npm -g root)"
antora --generator antora-site-generator-lunr antora-playbook.yml
